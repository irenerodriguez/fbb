#!/bin/bash
if [ -e ../bin/fbba ]
then
echo "fbba exist"
else 
echo "fbba no present in ../bin/"
echo "Compile with make"
cd ..
make
cd test
if [ -e ../bin/fbba ]
then
echo "fbba exist"
else
echo "Could not compile. Revise you have the boost libraries, g++ and make"
echo "exiting"
exit
fi
fi


if [ -f NC_001139.fna.gz ]
then
    gzip -d NC_001139.fna.gz
fi
if [ -f NC_001139.gff.gz ]
then
    gzip -d NC_001139.gff.gz
fi
if [ -f r1.fastq.gz ]
then
    gzip -d r1.fastq.gz
fi
if [ -f r2.fastq.gz ]
then
    gzip -d r2.fastq.gz
fi

rm gene_expression_ordered.dat
../bin/fbba -f NC_001139.fna -g NC_001139.gff -1 r1.fastq -2 r2.fastq -s results

if [ -f gene_expression_ordered.dat ]
then
    echo "The alignment takes 1.60 sec in AMD Opteron 6328  @ 3.20GHz"
    echo "The alignment takes 1.64 sec in Intel i7    970   @ 3.20GHz" 
    echo "The alignment takes 1.92 sec in MACIntel i7 2620  @ 2.70GHz" 
    echo "The alignment takes 2.23 sec in Intel Q9450       @ 2.66GHz"
    echo -e '\n'
    echo "*********Ordered genes ***********************"
    echo -e "Gene"'\t'"Total counted bps in gene/Gene lenght"
    echo -e "----"'\t'"-------------------------------------"
    head -15 gene_expression_ordered.dat
    echo "**********************************************"
    echo -e '\n'
else
    echo "An error ocurred during execution"
    exit
fi
if [ -f /usr/bin/xmgrace ]
then
echo "Let's look at fragment size"
xmgrace prob_d.dat &
fi

gzip r1.fastq
gzip r2.fastq
gzip NC_001139.fna
gzip NC_001139.gff
