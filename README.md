## Introduction

FBB (Fast Bayesian Bound) is a C++ software to reference RNA-seq aligners to the same canonical values based on a Bayesian bound.

FBB package provides two implementations of the multi-valued function used to efficiently indexing the genome/transcriptome:
* **FBBA_INDELS** is based on our own implementation of the muti-valued function.
* **FBBA_BUCKET_INDELS** uses the hash implementation provided by C++ Boost library (http://www.boost.org/), which is more efficient when there are many collisions.

We recommend to use FBBA_INDELS for small chromosomes (few collisions), and FBBA_BUCKET INDELS for large chromosomes (many collisions). The distinction between small and large cromosomes depends on the memory available in the computer/cluster where the code is going to be run. In our experiments, run in a Intel(R) Xeon(R) CPU E5-2680 0 @ 2.70GHz machine with 126GB, FBBA_INDELS worked well for Ecoli and Saccharomyces genomes, while FBBA_BUCKET_INDELS was more appropriate for Mus Musculus data. We recommend trying to use FBBA_INDELS whenever possible since its hash implementation was specially designed for the FBB problem; and use FBBA_BUCKET_INDELS, otherwise.


In turn, both implementations offer two main features:

* **fbb**: aligner comparator based on a the Fast Bayesian Bound (FBB). Given a SAM file, a FASTA file with the reference genome/transcriptome, and an annotation file (gff/gff3 format), FBB gives as output estimates of the FBB score, the false positive pairs, and the percentage of alignments to the incorrect strand. 
* **fbba**: aligner based on the optimization of the Fast Bayesian Bound (FBB) score. Given a paired-end reads (FASTQ files), a reference genome/transcriptome (FASTA), and an annotation file (gff/gff3), FBBA gives as output a SAM file with the alignments found by the FBBA mapper as well as estimates of the FBB score, the false positive pairs, and the percentage of alignments to the incorrect strand.


The following sections showing some examples of use are valid for any of the two versions: FBBA_INDELS or FBBA_BUCKET_INDELS.

## Installation

* FBBA_INDELS: To compile:
~~~~
make all
~~~~

* FBBA_BUCKET_INDELS: This version uses the boost libraries. So you need to install them first. This is the location (http://www.boost.org/). Then,
~~~~
make all
~~~~




## FBB


### Input

~~~~
* * * Fast Bayesian Bound (FBB) using Quality Scores * * * 

This software is freely available for academic or commercial use under the terms of the GNU General Public Licence (see gpl.txt).

Usage: fbb [options]
options:
-t Alignment Threshold (default -100)
-f Genome/Chromosome/Transcriptome file in FASTA format
-g Annotation file in GFF format
-G Annotation file in GFF3 format
-C Chromosome name in GFF3 
-s : input file in SAM format
~~~~

The alignment threshold determines the minimum value for the FBB score to have a valid alignmetn. It can take any negative value (logarithmic scale).


### Output

As output, the **fbb** program provides the following files:

* cdf_d.dat: Empirical cumulative distribution function for the paired-end read distances P(d).
* gene_expression.dat: Gene expression for the genes in the GFF/GFF3 file.
* gene_expression_ordered.dat: Gene expression for the genes in the GFF/GFF3 file in decreasing order.
* Main_metrics.dat: alignment rate according to FBB criterion, percentage of false paired-end alignments, percentage of alignments to the incorrect strand, milliseconds for computing FBB scores, <FBB> score (average FBB among alignments).
* prob_d.dat: Empirical probability distribution for the paired-end read distances P(d).

### Test

In the subfolder data, you can find a SAM file obtained from (unstranded) simulated data on E. Coli genome. You can run the FBB program as follows from data subfolder:

~~~~
../FBBA_INDELS/bin/fbb -t -100 -f NC_000913.transcriptome -g NC_000913.gff -s simulations_100_0_0.sam
~~~~

**NOTE**: Errors due to alignment to the incorrect strand should only be considered for stranded data and when providing the annotation file associated with the FASTA file. Results on gene expression are only valid when positions in the anotation file corresponds to positions in the FASTA file.

In the subfolder data, you can find another SAM file with Bowtie exact alignments for real stranded data (E. Coli genome). You can run the FBB program as follows form data subfolder:

~~~~
../FBBA_INDELS/bin/fbb -t -100 -f NC_000913.fna -g NC_000913.gff -s bowtie.sam
~~~~

In this case, as it is stranded data and the annotation file corresponds to the FASTA file, errors due to alignment to the incorrect strand and gene expression levels are valid.

You can run the same examples using the **FBBA_BUCKET_INDELS** implementation.

## FBBA

### Input


~~~~
* * * Fast Bayesian Bound Alignment (FBBA) using Quality Scores * * * 

This software is freely available for academic or commercial use under the terms of the GNU General Public Licence (see gpl.txt).

Usage: fbba [options]
options:
-t Alignment Threshold (default -100)
-f Genome/Chromosome/Transcriptome file in FASTA format
-g Annotation file in GFF format
-G Annotation file in GFF3 format
-C Chromosome name in GFF3 
-1 Short reads file 1
-2 Short reads file 2
-s : output file in SAM format
~~~~


-1 and -2 options are used to indicate the name of FASTQ files with RNA-seq paired-end reads.


### Output

As output, the **fbba** program provides the following files:

* SAM file (-s option) with FBBA alignments.
* cdf_d.dat: Empirical cumulative distribution function for the paired-end read distances P(d).
* gene_expression.dat: Gene expression for the genes in the GFF/GFF3 file.
* gene_expression_ordered.dat: Gene expression for the genes in the GFF/GFF3 file in decreasing order.
* Main_metrics.dat: alignment rate according to FBB criterion, percentage of false paired-end alignments, percentage of alignments to the incorrect strand, milliseconds for computing FBB scores, <FBB> score (average FBB among alignments).
* prob_d.dat: Empirical probability distribution for the paired-end read distances P(d).

### Test

You can find an example in FBBA_INDELS/test subfolder. Do the following:

~~~~
cd test
./run_example.bash
~~~~

You can run the same examples using the **FBBA_BUCKET_INDELS** implementation.


## Bug Reports

Bug reports are very welcome as issues on our [Bitbucket repository](https://bitbucket.org/irenerodriguez/fbb/)



