/**************************************************************/
/************* FASTQ           PREP              **************/
/**************************************************************/
/***** Started 8/24/2013   Ramon Huerta************************/
/**************************************************************/

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set>
//#include <hash_map>
#include <iterator>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cstdio>
#include <cstring>

using namespace std;
#include "load_parameters.h"
#include "fbbas.h"

int main(int argc,char *argv[]) {
  param fbba;
  
  parse_cmd(argc, argv,&fbba,"fbba");
 if (fbba.fna.size()==0) {
    puts("FNA file not present");
    bad_cmd_fbba();
    exit(0);
  }

 if ( (fbba.gff.size()!=0)) {
    puts("gff file present");
    //bad_cmd_fbba();
  
 } else if (fbba.gff3.size()!=0) {
    puts("gff3 file present");
   } else {
     bad_cmd_fbba();
   }

 if (fbba.read1.size()==0) {
    puts("file 1 of short reads file not present");
    bad_cmd_fbba();
    exit(0);
  }
   if (fbba.read2.size()==0) {
    puts("file 2 of short reads file not present");
    bad_cmd_fbba();
    exit(0);
  }
  fastq A(&fbba);
  if ( (fbba.gff.size()!=0)) {
    puts("Loading gff file");
    A.read_gff();
  } else if ( (fbba.gff3.size()!=0)) {
    puts("Loading gff3 file");
    A.read_gff3();
  } else {
    puts("No gff files");
    exit(0);
  }
  puts("Indexing");
  A.indexing();
  puts("Pairing reads");
  //A.readq_double(&fbba);
  A.readq_double_full(&fbba);

  return 0;
}
