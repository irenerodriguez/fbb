#include<map>
#include<set>
//#include<string>
#include<iostream>
//#include <x86intrin.h>
//#include <inttypes.h>
//#include<unordered_map>
//#include<boost/unordered_map.hpp>
//#include <boost/functional/hash.hpp>
//#include <google/sparse_hash_map>
//#include <google/dense_hash_map>

#define NBIT 2

//  if __WORDSIZE == 64
// #define LONG_MAX	9223372036854775807L

#define BUFFERSIZE  4096
//If the short reads are longer you need to increase this appropiately
#define LOCALBUFFER 208

// Max distance in bps
#define MAX_DISTANCE 3000


using namespace std;

#define changeB(X) ((X=='T')?'A':((X=='A')?'T':((X=='G')?'C':'G')))
#define ABS(X) (((X)>0)?(X):(-(X)))
#define MMIN(X,Y) (((X)<(Y))?(X):(Y))
#define MMAX(X,Y) (((X)>(Y))?(X):(Y))

typedef struct {
  int n;
  float *uf;
  int *loc;
  char *near;

  int nu; //For local calculations only
  float *U; //For local calculations only
  int *locU; // For local calculations only

  int nuF; // To accumulate global
  float *UF; // To accumulate global
  int *locUF; //to accumulate global

  // int nnu;
  int  *nU;  //For local calculations only

  //set<int> i;

} trackindex;

typedef struct {
  void *p;
  unsigned long i; 
  } node; 

typedef struct {
  void *p;
  unsigned long N;
} root;
 
//typedef unordered_map<unsigned long, vector<unsigned int> > maphI;
//typedef boost::unordered_map<unsigned long, vector<unsigned int> > maphI;
//typedef google::sparse_hash_map<unsigned long, vector<unsigned int> > maphI;

typedef struct {
  char strand;
  char gene_name[200];
  long ini;
  long fin;
  string gene;
  string promoter;
  int L;
} Fieldgene;

#define OJUMP 3 
typedef struct {
  string qname;
  int flag;
  string rname;
  int    pos;
  int    mapq;
  string cigar;
  string rnext;
  string pnext;
  int    tlen;
  string seq;
  string qual;
} samfields;

class fastq {
 private:
  int Nb; //Fixing number of bases in index

  string GFF_file;
  string GFF3_name;
  string chromosome_name;
  unsigned char KMER;
  unsigned long N;
  unsigned long Np; // Max size of index
  root *  index;
  //maphI indHI;

  unsigned long total_entries;
  set<unsigned long> present; // locatios in index that are used 
  unsigned long  * Pref;

  map<long,string> fastaheader;
  bool samfile;
  samfields S1,S2;

  //int minsegment;
  int qmin;

  float * qtop;

  char *  seq; //genomic sequence
  //char *fseq; //genomic positions
  unsigned long  *srep;
  unsigned long  *srepup;
  unsigned long  *srepdown;
  unsigned long lower_mask;

  long total_bps_reads;

  float *score; //score
  bool withfastq;
  string Experiment_name;
  map<float,int> blastn_stats;

  
  float qbase; // Base value for no hits in the aligment algorithm
  int offset;  // For the distribution
  float align_rate;
  float false_positive;
  float false_pairs;
  float align_time;
  float A_max_score; //Storing average BBA rate

  //Genes
  map<unsigned long,vector<string> > map_to_gene;
  map<unsigned long,string> map_to_gene_pos;
  map<unsigned long,string> map_to_gene_neg; // map from genome position to gene name

  map<string,long> Lgene;
  map<string,Fieldgene> Agene; //all gene data
  map<string,double> Gcounts,G_pos,G_neg;
  multimap<double,string> OGcounts; // ordered list with gene counts

  long ngenes;
  long ngenesP;
  long ngenesN;
  long *lgene;
  long *lncP; //lengh non coding region positive strand
  long *lgeneP; //lengh coding region positive strand
  long *lncN; 
  long *lgeneN;
  char *strand_sign;
  unsigned long total_coding_bps;
  float log_total_coding_bps;
  float logN;
  multimap<long,long > domainP,domainN; // Coding the genome positions of the coding regions
                                        // For positive and negative strands
 
  char **gene; 
  char **geneP; //coding region positive strand
  char **geneN; //coding region negative strand
  char **noncP; //non coding region positive strand
  char **noncN; //non coding region negative strand

  vector<samfields> LR,RR;
  vector<char> complementL,complementR;

 public:
  fastq(param *);
  fastq( const string);
  void read_genes(char *,int); 
  void read_genes(char *); 
  bool splitgene(char *,Fieldgene *);
  //time_t sstime();
  void indexing();
  void indexing2();
  void reverse(char *,long);
  void reverseM(char *,char *,long);
  void reverseD(char *,char *,long);
  void reverseS(float *,float *,long);
  void readbuffer(char **,char *);
  unsigned char ecode(char );
  bool encoding(char *,unsigned long &);
  void readq_scores(char *);
  void readq_double_full(const param *);
  void readq_double_sam(const param *);
  void readq_double_calibration(const param *);
  void readq_theorem1(const param *);
  //void readq_double_deterministic(char *,char *);
  unsigned long best_match_q(char *,float *,int, float *,bool &,float  ) ;
  //  void best_match_q(char *,float *,int, float *,bool &,float, boost::unordered_map<unsigned long,float> & ) ;
  // void best_match_q(unsigned long *,float *,int, float *,bool &,float, boost::unordered_map<unsigned long,float> & ) ;
  float best_match_q(unsigned long *,float *,int, float *,bool &,float, trackindex & ) ;
  
  void  form_qs(char *,float *,float *,int,float &);
  float arate();
  float fdr();
  float falserate();
  float aligntime();
  void read_gff() ;
  void read_gff(char *,int);
  void read_gff(char *,string);
  void read_gff_str(char *,string);
  void read_gff3();
  string check_gene_pos(unsigned long );
  string check_gene_neg(unsigned long );
  vector<string> check_gene(unsigned long );
  void save_gene_expression();
  void count_gene_expression();
  void count_gene_expression_gross();
  //unsigned int best_match_deterministic(char *,int,bool &);
  bool chromosome(char *,int &) ;
  bool chromosome(char *,string &) ;
  void show_aligment(char *,unsigned long,int);
  multimap<double,string> get_gene_counts();
  void clear_gene_counts();
  //This will become deprecated
  //float eval_best(boost::unordered_map<unsigned long,float> &,boost::unordered_map<unsigned long,float> &,vector<int> &,int,unsigned long &,unsigned long &,float,bool);
  // New replacemente
  bool eval_g_pos(trackindex &,int , string) ;
  float eval_best(trackindex &,trackindex &,vector<int> &,int,unsigned long &,unsigned long &,float,bool);
   float eval_best_sam(trackindex &,trackindex &,vector<int> &,int,unsigned long &,unsigned long &,float,bool);
  
  //  float eval_best_theorem1(boost::unordered_map<unsigned long,float> &,boost::unordered_map<unsigned long,float> &,vector<int> &,int,unsigned long &,unsigned long &,float,bool);
  

  void save_distribution(vector<int>&,int,int) ;
  ~fastq();
  void count_entries();
  void reindexing();
  void save_index(string); 
  void read_index(string);
  void supercode(char *,unsigned long *,int );
  void reserve_memorylocal(trackindex &); 
  void release_localmemory(trackindex &);
  void reserve_memory(unsigned long );
  void read_sam(string); 
  void save_main_metrics(char *);
  float evalD(int,vector<int> &,int);
  int get_relative_position(int pos);
};
