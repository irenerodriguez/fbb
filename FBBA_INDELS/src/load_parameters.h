#include <vector>
#include <string>
using namespace std;

typedef struct {
  vector<string> gff_files;
  vector<string> fna_files;
  int bline; //Non-overlapping genes margin
  string gff;
  string gff3;
  string chromo;
  string fna;
  int fna_length;
  float threshold;
  string read1;
  string read2;
  bool samfile;
  string samfile_name;
  string indexfile;
  int Nb;
} param;

void parse_cmd(int, char **,param *,string type);
void bad_cmd_fbba();
void bad_cmd_fbb();
